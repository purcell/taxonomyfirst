<?php

namespace Drupal\tsa_migrate_example\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;

/**
 * This is an example of a simple SQL-based source plugin.
 *
 * Source plugins are classes which deliver source data to the processing
 * pipeline. For SQL sources, the SqlBase class provides most of the
 * functionality needed - for a specific migration, you are required to
 * implement the three simple public methods you see below.
 *
 * This annotation tells Drupal that the name of the MigrateSource plugin
 * implemented by this class is "beer_term". This is the name that the migration
 * configuration references with the source "plugin" key.
 *
 * @MigrateSource(
 *   id = "tsa_term_example"
 * )
 */
class TsaTermExample extends SqlBase {
  /**
   * {@inheritdoc}
   */
  public function query() {
    $fields = [
      'name',
      // 'machine_name',
      'description',
      'vid',
    ];
    return $this->select('taxonomy_term_data', 'tax_data')
      ->fields('tax_data', $fields)
      // This sort assures that parents are saved before children.
      ->condition('vid', 21, '=');
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'name' => $this->t('Category name'),
      'description' => $this->t('Description of the category'),
      // 'machine_name' => $this->t('Machine name'),
      'vid' => $this->t('ID of the parent category'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return ['vid' => ['type' => 'integer']];
  }

}
